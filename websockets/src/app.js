"use strict";
import dotenv from "dotenv";
import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import path from "path";

dotenv.config();

const app = express();
const http = createServer(app);
const io = new Server(http);
const port = process.env.PORT || 3000;
const __dirname = path.resolve();

app.use(express.static("public"));

app.get("/", function (_, res) {
  res.sendFile(path.join(__dirname, "public/html/index.html"));
});

app.get("/join", function (req, res) {
  res.sendFile(path.join(__dirname, "public/html/chat.html"));
});

io.on("connection", (socket) => {
  console.log("a user connected", socket.id);
  socket.on("join", (room) => {
    socket.join(room);
  });

  socket.on("disconnect", () => {
    console.log("a user disconnected", socket.id);
  });

  socket.on("msg", (obj) => {
    console.log("message:", obj);
    io.in(obj.room).emit("msg", { user: obj.user, msg: obj.msg });
  });
});

http.listen(port, () => {
  console.log(`Socket.io chat app listening on port ${port}!`);
});
