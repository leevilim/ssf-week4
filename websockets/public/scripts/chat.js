"use strict";

const socket = io();

const url = new URL(window.location.href);

const room = url.searchParams.get("room");
const user = url.searchParams.get("user");

socket.emit("join", room);

document.querySelector("form").addEventListener("submit", (event) => {
  event.preventDefault();
  const inp = document.getElementById("m");
  socket.emit("msg", {
    user: user,
    room: room,
    msg: inp.value,
  });
  inp.value = "";
});

socket.on("msg", (msg) => {
  const item = document.createElement("li");
  item.innerHTML = msg.user + ": " + msg.msg;
  const list = document.getElementById("messages");
  list.appendChild(item);
  list.scrollTop = list.scrollHeight;
});
