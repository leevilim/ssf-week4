import mongoose from "mongoose";

const userModel = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.ObjectId,
    auto: true,
  },
  username: String,
  password: String,
});

export default mongoose.model("User", userModel);
