import jwt from "jsonwebtoken";
import passport from "passport";

export const checkAuth = (req, res) => {
  return new Promise((resolve) => {
    passport.authenticate("jwt", (_, user, err) => {
      if (err) {
        return resolve(false);
      }
      return resolve(user);
    })(req, res);
  });
};

export const loginUser = async (req, res, args) => {
  req.body.username = args.username;
  req.body.password = args.password;

  return await new Promise((resolve) =>
    passport.authenticate("local", (err, user, info) => {
      if (err || !user) {
        resolve(false);
      }
      const token = jwt.sign(user, "testing");
      resolve({ ...user, token: token });
    })(req, res)
  );
};
