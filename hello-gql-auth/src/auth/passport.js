import passport from "passport";
import passportJWT from "passport-jwt";
import { Strategy } from "passport-local";
import User from "../models/userModel.js";
import bcrypt from "bcryptjs";

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(
  new Strategy(async (username, password, done) => {
    try {
      const user = await User.findOne({ username }).lean();
      if (user === undefined) {
        return done(null, false, { message: "Incorrect email." });
      }

      if (!bcrypt.compareSync(password, user.password)) {
        return done(null, false, { message: "Incorrect password." });
      }
      delete user.password;
      return done(null, { ...user }, { message: "Logged In Successfully" });
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: "testing",
    },
    async (jwtPayload, done) => {
      try {
        const user = await User.findById(jwtPayload._id).lean();
        if (user === undefined) {
          return done(null, false);
        }
        delete user.password;
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);

export default passport;
