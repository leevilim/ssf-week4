import Animal from "../models/animalModel.js";
import Species from "../models/speciesModel.js";
import mongoose from "mongoose";
import { AuthenticationError } from "apollo-server-errors";

export default {
  Query: {
    animals: async (_, args, context) => {
      // In this case, we'll pretend there is no data when
      // we're not logged in. Another option would be to
      // throw an error.
      if (!context.user) throw new AuthenticationError();

      return await Animal.find({}).exec();
    },
  },

  Animal: {
    species: async (parent, _) => {
      const id = parent.species;
      return await Species.findById(id).exec();
    },
  },

  Mutation: {
    addAnimal: async (_, args) => {
      const animal = new Animal(args);
      return await animal.save();
    },

    modifyAnimal: async (_, args) => {
      console.log(args);

      return Animal.findByIdAndUpdate(args.id, {
        animalName: args.animalName,
        species: mongoose.Types.ObjectId(args.species),
      });
    },
  },
};
