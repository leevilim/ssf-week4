import User from "../models/userModel.js";
import bcrypt from "bcryptjs";
import { loginUser } from "../auth/func.js";

export default {
  Query: {
    user: async (parent, args, { user }) => {
      return user;
    },
    login: async (parent, args, { req, res }) => {
      return await loginUser(req, res, args);
    },
  },

  Mutation: {
    registerUser: async (parent, args) => {
      try {
        const hash = await bcrypt.hash(args.password, 12);
        const userWithHash = {
          ...args,
          password: hash,
        };
        const newUser = new User(userWithHash);
        const result = await newUser.save();
        return result;
      } catch (err) {
        throw new Error(err);
      }
    },
  },
};
