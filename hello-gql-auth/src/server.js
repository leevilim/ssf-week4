import { ApolloServer } from "apollo-server-express";
import typeDefs from "./schemas/index.js";
import resolvers from "./resolvers/index.js";
import express from "express";
import db from "./utils/db.js";
import { checkAuth } from "./auth/func.js";
import passport from "./auth/passport.js";

(async () => {
  try {
    const server = new ApolloServer({
      typeDefs,
      resolvers,
      context: async ({ req, res }) => {
        if (req) {
          const user = await checkAuth(req, res);
          return {
            req,
            res,
            user,
          };
        }
      },
    });

    const app = express();
    app.use(passport.initialize());

    await server.start();

    server.applyMiddleware({ app });

    db.on("connected", () => {
      app.listen({ port: process.env.PORT || 3000 }, () =>
        console.log(
          `🚀 Server ready at http://localhost:3000${server.graphqlPath}`
        )
      );
    });
  } catch (e) {
    console.log("server error: " + e.message);
  }
})();
