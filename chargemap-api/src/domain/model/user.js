import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
  _id: { type: mongoose.Schema.ObjectId, auto: true },
  Username: String,
  Password: String,
  Full_name: String,
});

export default mongoose.model("User", userSchema);
