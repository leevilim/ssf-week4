import mongoose from "mongoose";

const connectionSchema = new mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  ConnectionTypeID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "ConnectionType",
  },
  LevelID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Level",
  },
  CurrentTypeID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "CurrentType",
  },
  Quantity: Number,
});

export default mongoose.model("Connection", connectionSchema);
