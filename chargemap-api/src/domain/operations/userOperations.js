import User from "../model/user.js";
import bcrypt from "bcryptjs";

export const getUserByName = async (username) => {
  const user = await User.find({ username: username }).lean();
  return user[0] ?? null;
};

export const getUserById = async (id) => {
  const user = await User.findById(id);

  return user ?? null;
};

export const createUser = async (args) => {
  console.log(args);
  try {
    const hash = await bcrypt.hash(args.Password, 12);
    const userWithHash = {
      ...args,
      Password: hash,
    };
    const newUser = new User(userWithHash);
    const result = await newUser.save();
    return result;
  } catch (err) {
    console.log("Failed to create user:", err);
    return null;
  }
};
