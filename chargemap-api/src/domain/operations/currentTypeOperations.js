import CurrentType from "../model/currentType.js";

export const getCurrentType = async (id) => {
  return await CurrentType.findById(id);
};

export const getCurrentTypes = async () => {
  return await CurrentType.find({});
};
