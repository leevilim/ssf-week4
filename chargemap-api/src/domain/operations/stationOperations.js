import Station from "../model/station.js";
import { rectangleBounds, renameKey } from "../../utils/other.js";
import {
  createConnections,
  updateOrCreateConnections,
} from "./connectionOperations.js";
import mongoose from "mongoose";

export const getStationById = async (id) => {
  return await Station.findById(id);
};

export const queryStations = async (params = {}) => {
  return await Station.find(buildStationQuery(params)).setOptions(
    buildStationQueryOptions(params)
  );
};

export const createStation = async (data) => {
  const connectionIds = await createConnections(data.Connections);
  const station = data;
  const createRes = await Station.create({
    _id: mongoose.Types.ObjectId(),
    Location: {
      type: "Point",
      coordinates: station.Location.coordinates,
    },
    Connections: connectionIds,
    Title: station.Title,
    AddressLine1: station.AddressLine1,
    Town: station.Town,
    StateOrProvince: station.StateOrProvince,
    Postcode: station.Postcode,
  });

  return createRes;
};

export const updateStation = async (data) => {
  const connectionIds = await updateOrCreateConnections(data.Connections);
  const station = data;

  const updateRes = await Station.findByIdAndUpdate(
    station.id,
    {
      Connections: connectionIds,
      Title: station.Title,
      AddressLine1: station.AddressLine1,
      Town: station.Town,
      StateOrProvince: station.StateOrProvince,
      Postcode: station.Postcode,
    },
    { new: true }
  );

  return updateRes;
};

export const deleteStationById = async (id) => {
  await Station.findByIdAndDelete(id);
  return "Deleted.";
};

const buildStationQuery = (params) => {
  const query = {};
  const containsCoords =
    params._southWest !== undefined && params._northEast !== undefined;

  if (containsCoords) {
    query.Location = {
      $geoWithin: {
        $geometry: rectangleBounds(params._northEast, params._southWest),
      },
    };
  }
};

const buildStationQueryOptions = (params) => {
  return {
    limit: params.limit || 10,
    skip: params.start || 0,
  };
};
