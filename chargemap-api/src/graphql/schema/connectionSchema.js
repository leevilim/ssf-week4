import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    Connections: [Connection]
  }

  input ConnectionInput {
    ConnectionTypeID: String
    LevelID: String
    CurrentTypeID: String
    Quantity: Int
  }

  type Connection {
    id: ID
    ConnectionTypeID: ConnectionType
    LevelID: Level
    CurrentTypeID: CurrentType
    Quantity: Int
  }
`;
