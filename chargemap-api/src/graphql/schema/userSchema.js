import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    user: User
    login(Username: String!, Password: String!): User
  }

  extend type Mutation {
    registerUser(Username: String!, Password: String!): User
  }

  type User {
    id: ID
    Username: String
    Password: String
    Full_name: String
    Token: String
  }
`;
