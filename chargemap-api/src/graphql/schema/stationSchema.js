import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    stations(limit: Int, start: Int, bounds: Bounds): [Station]
    station(id: ID): Station
  }

  extend type Mutation {
    addStation(
      Connections: [ConnectionInput]
      Postcode: String
      Title: String
      AddressLine1: String
      StateOrProvince: String
      Town: String
      Location: LocationInput
    ): Station

    modifyStation(
      id: ID
      Connections: [ConnectionInput]
      Postcode: String
      Title: String
      AddressLine1: String
      StateOrProvince: String
      Town: String
      Location: LocationInput
    ): Station

    deleteStation(id: ID): Station
  }

  type Station {
    id: ID
    Location: Location
    Connections: [Connection]
    Title: String
    AddressLine1: String
    Town: String
    StateOrProvince: String
    Postcode: String
  }

  type Location {
    type: String!
    coordinates: [Float!]
  }

  input LocationInput {
    coordinates: [Float!]
  }

  input Bounds {
    _southWest: Coord
    _northEast: Coord
  }

  input Coord {
    lat: Float
    lng: Float
  }
`;
