import { getConnectionTypes } from "./../../Domain/Operations/connectionTypeOperations.js";

export default {
  Query: {
    connectiontypes: async (parent, args) => {
      return await getConnectionTypes();
    },
  },
};
