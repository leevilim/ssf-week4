import { loginUser } from "../../auth/func.js";
import { createUser } from "../../domain/operations/userOperations.js";

export default {
  Query: {
    user: async (parent, args, { user }) => {
      return user;
    },
    login: async (parent, args, { req, res }) => {
      return await loginUser(req, res, args);
    },
  },

  Mutation: {
    registerUser: async (parent, args) => {
      return await createUser(args);
    },
  },
};
