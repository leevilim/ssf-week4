import { getLevels } from "./../../domain/operations/levelOperations.js";

export default {
  Query: {
    leveltypes: async (_, args) => {
      return await getLevels();
    },
  },
};
