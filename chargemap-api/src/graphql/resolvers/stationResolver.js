import {
  getStationById,
  queryStations,
  createStation,
  updateStation,
  deleteStationById,
} from "../../domain/operations/stationOperations.js";
import mongoose from "mongoose";
import { getConnection } from "../../domain/operations/connectionOperations.js";
import { AuthenticationError } from "apollo-server-errors";

export default {
  Query: {
    station: async (_, args) => {
      const id = args.id;
      return await getStationById(mongoose.Types.ObjectId(id));
    },
    stations: async (_, args) => {
      return await queryStations(args);
    },
  },

  Mutation: {
    addStation: async (_, args, { user }) => {
      if (!user) {
        throw new AuthenticationError();
      }
      return await createStation(args);
    },

    modifyStation: async (_, args, { user }) => {
      if (!user) {
        throw new AuthenticationError();
      }
      return await updateStation(args);
    },

    deleteStation: async (_, args, { user }) => {
      if (!user) {
        throw new AuthenticationError();
      }
      return await deleteStationById(mongoose.Types.ObjectId(args.id));
    },
  },

  Station: {
    Connections: async (parent, args) => {
      return Promise.all(
        parent.Connections.map(async (id) => {
          return await getConnection(id);
        })
      );
    },
  },
};
