import { ApolloServer } from "apollo-server-express";
import typeDefs from "./graphql/schema/index.js";
import resolvers from "./graphql/resolvers/index.js";
import express from "express";
import db from "./utils/db.js";
import cors from "cors";
import { checkAuth } from "./auth/func.js";
import passport from "./auth/passport.js";
import https from "https";
import http from "http";
import fs from "fs";
import helmet from "helmet";

const sslkey = fs.readFileSync("./ssl-key.pem");
const sslcert = fs.readFileSync("./ssl-cert.pem");
const options = {
  key: sslkey,
  cert: sslcert,
};

(async () => {
  try {
    const server = new ApolloServer({
      typeDefs,
      resolvers,
      context: async ({ req, res }) => {
        if (req) {
          const user = await checkAuth(req, res);
          return {
            req,
            res,
            user,
          };
        }
      },
    });

    const app = express();
    app.use(cors({ origin: true }));
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(passport.initialize());
    app.use(helmet());

    await server.start();

    server.applyMiddleware({ app });

    db.on("connected", () => {
      http
        .createServer((req, res) => {
          console.log("REDIR");
          res.writeHead(307, {
            Location: `https://${req.headers.host}:443${server.graphqlPath}`,
          });
          res.end();
        })
        .listen(80);

      https
        .createServer(options, app)
        .listen({ port: process.env.PORT || 443 }, () =>
          console.log(`🚀 Server ready`)
        );
    });
  } catch (e) {
    console.log("server error: " + e.message);
  }
})();
